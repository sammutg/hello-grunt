# Projet1

The best jQuery plugin ever.

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/samgu/projet1/master/dist/projet1.min.js
[max]: https://raw.github.com/samgu/projet1/master/dist/projet1.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/projet1.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
