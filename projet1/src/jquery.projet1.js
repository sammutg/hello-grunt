/*
 * projet1
 * 
 *
 * Copyright (c) 2014 Sammut Guillaume
 * Licensed under the samgu license.
 */

(function($) {

  // Collection method.
  $.fn.projet1 = function() {
    return this.each(function(i) {
      // Do something awesome to each selected element.
      $(this).html('awesome' + i);
    });
  };

  // Static method.
  $.projet1 = function(options) {
    // Override default options with passed-in options.
    options = $.extend({}, $.projet1.options, options);
    // Return something awesome.
    return 'awesome' + options.punctuation;
  };

  // Static method default options.
  $.projet1.options = {
    punctuation: '.'
  };

  // Custom selector.
  $.expr[':'].projet1 = function(elem) {
    // Is this element awesome?
    return $(elem).text().indexOf('awesome') !== -1;
  };

}(jQuery));
